export interface ProductInterface {
  price: number;
  id: number;
  img: string;
  name: string;
}

export class ProductModel {
  price: number;
  id: number;
  img: string;
  name: string;

  constructor(data: ProductInterface) {
    this.price = data.price || null;
    this.id = data.id || null;
    this.img = data.img || null;
    this.name = data.name || null;
  }

  get summary() {
    return `${this.name} ${this.price}`;
  }

}

const products: ProductInterface[] = [
  {
    price: 1099.00,
    id: 1,
    img: 'https://i.linio.com/p/009c9acfe0773aabe6ac2068d6de3c36-card.jpg',
    name: 'Laptop HP 250 G6 Intel Core i3 1TB 4GB RAM - FreeDOS'
  },
  {
    price: 189.00,
    id: 2,
    img: 'https://i.linio.com/p/d2bce90449a51110e3fd3aa39daa0473-card.jpg',
    name: 'Disco Duro Externo Western Digital 1TB  + Funda-Negro'
  },
  {
    price: 79.00,
    id: 3,
    img: 'https://i.linio.com/p/9f3edf9561e0555dd915e2fc5464fb13-card.jpg',
    name: 'Audífono Sennheiser HD 206 negro'
  },
  {
    price: 69.00,
    id: 4,
    img: 'https://i.linio.com/p/da2300eed9bbb2147de7b5b23776a9ce-card.jpg',
    name: 'Membresía Linio Plus 1 año'
  },
  {
    price: 1285.00,
    id: 5,
    img: 'https://i.linio.com/p/0e4306e31e22f809545331b1ac519bf6-card.jpg',
    name: 'Ps4 Consola Play Station 4 Slim - Negro - 1 TB'
  },
];

export class MockDb {

  static getProduct() {
    return products;
  }

  static findProduct(id: number) {
    return products.find(item => item.id === id);
  }
}

