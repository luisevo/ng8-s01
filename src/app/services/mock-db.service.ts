import { Injectable } from '@angular/core';
import {MockDb, ProductInterface, ProductModel} from '../utils/mock-db';
import {Observable, of} from 'rxjs';
import {map, timeout} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MockDbService {

  constructor() { }

  getProducts(): Observable<ProductModel[]> {
    return of(MockDb.getProduct())
      .pipe(
        map(res => res.map(product => new ProductModel(product)) ),
        timeout(5000)
      );
  }

  findProduct(id: number): Observable<ProductInterface> {
    return of(MockDb.findProduct(id));
  }
}
