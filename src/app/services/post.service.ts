import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from "rxjs";
import {PostInterface} from "../interfaces/post.interface";
import {PostModel} from "../models/post.model";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(
    private http: HttpClient
  ) {}

  getAll(): Observable<PostModel[]> {
    return this.http.get<PostInterface[]>('https://jsonplaceholder.typicode.com/posts')
      .pipe(
        map((res) => res.map(item => new PostModel(item))),
      );
  }

}
