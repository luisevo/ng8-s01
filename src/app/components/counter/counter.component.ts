import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.scss']
})
export class CounterComponent implements OnInit {
  @Input() counter: number;
  @Output() counterChange: EventEmitter<number> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  add() {
    this.counter += 1;
    this.counterChange.emit(this.counter);
  }

  less() {
    this.counter -= 1;
    this.counterChange.emit(this.counter);
  }

}
