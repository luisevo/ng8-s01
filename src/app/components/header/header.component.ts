import {Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges, DoCheck} from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy, OnChanges, DoCheck {
  // @Input() title; mismo nombre del atributo que recibimos
  @Input('title') headerTitle: string;
  @Input() color: string;
  subtitle: string; // no input

  constructor() {
    this.headerTitle = 'Titulo por defecto';
    this.color = 'light';
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    // changes : {
    // previousValue: any;
    // currentValue: any;
    // firstChange: boolean; }
  }

  ngDoCheck(): void {
    console.log(this.subtitle);
  }

  ngOnDestroy() {
  }

}
