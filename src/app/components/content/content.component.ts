import {AfterContentInit, AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit, AfterViewInit, AfterContentInit {
  // $('#Container')
  @ViewChild('Container', { static: true }) container: ElementRef;

  constructor() { }

  ngOnInit() {
    console.log(this.container); // null
  }

  ngAfterViewInit(): void {
    console.log(this.container);
    // despues que la vista a cargado
  }

  ngAfterContentInit(): void {
    // despues que la proyección de contenidos
  }

}
