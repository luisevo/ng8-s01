import {PostInterface} from "../interfaces/post.interface";

export class PostModel {
  body: string;
  title: string;
  id: number;
  user: { id: number };

  constructor(data: PostInterface) {
    this.body = data.body || null;
    this.title = data.title || null;
    this.id = data.id || null;
    this.user = data.userId ? { id: data.userId } : null;
  }

  get userId() {
    return this.user ? this.user.id : null;
  }
}

