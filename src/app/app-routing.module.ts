import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './pages/home/home.component';
import {ConfigComponent} from './pages/config/config.component';
import {ProductPageComponent} from './pages/product-page/product-page.component';

const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'}, // ruta base
  {path: 'home', component: HomeComponent},
  {path: 'config', component: ConfigComponent},
  // {path: 'product', component: ProductPageComponent}, // parametro opcional, 2 rutas
  {path: 'product/:id', component: ProductPageComponent},
  {path: '**', redirectTo: 'home'} // rutas desconocidas
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
