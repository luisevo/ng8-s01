import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ProductInterface} from '../../utils/mock-db';
import {MockDbService} from '../../services/mock-db.service';

@Component({
  selector: 'app-product-page',
  templateUrl: './product-page.component.html',
  styleUrls: ['./product-page.component.scss']
})
export class ProductPageComponent implements OnInit {
  product: ProductInterface;

  constructor(
    private router: Router, // para navegación
    private activatedRoute: ActivatedRoute, // para acceder a los parametros
    private mockDb: MockDbService
  ) { }

  ngOnInit() {
    // query params
    // const id = this.activatedRoute.snapshot.queryParamMap.get('id');
    // parametros
    const id = this.activatedRoute.snapshot.paramMap.get('id');

    // this.product = this.mockDb.findProduct(+id);
    console.log(this.product);
  }

}
