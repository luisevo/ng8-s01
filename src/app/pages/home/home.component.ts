import { Component, OnInit } from '@angular/core';
import {ProductInterface, ProductModel} from '../../utils/mock-db';
import {MockDbService} from '../../services/mock-db.service';
import {PostService} from "../../services/post.service";
import {PostInterface} from "../../interfaces/post.interface";
import {PostModel} from "../../models/post.model";
import {forkJoin} from "rxjs";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  products: ProductModel[] = [];
  posts: PostModel[] = [];

  constructor(
    private mockDb: MockDbService,
    private postService: PostService,
  ) { }

  ngOnInit() {
    this.postService.getAll()
      .subscribe(
        (res) => {
          this.posts = res;
        },
        err => console.log(err),
      );

    /* Multiples servicios
    forkJoin(
      this.mockDb.getProducts(),
      this.postService.getAll()
    ).subscribe(
      (res) => {
        this.products = res[0];
        this.posts = res[1];

      },
      err => console.log(err),
    );
    */

    /*
    this.mockDb.getProducts()
      .subscribe(
        (res) => {
          this.products = res;
          console.log('res', res);
        },
        (err) => {
          console.log(err);
        },
        () => {
          console.log('complete');
        }
      );
      */
  }

}
