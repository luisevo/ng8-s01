import { Component } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  counter = 1;
  dymanicClass = false;
  dymanicStyle = 'white';
  showInfo = false;
  items = [
    { title: 'item 1'},
    { title: 'item 2'},
  ];

  menuOptions = [
    { title: 'Inicio', path: 'home' },
    { title: 'Configuraciones', path: 'config' }
  ];

  content: string;

  constructor(
    private router: Router
  ) {}

  /*
  optionSelected(path: string) {
    this.router.navigateByUrl(path);
  }
  */
}
