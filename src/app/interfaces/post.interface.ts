export interface PostInterface {
  body: string;
  title: string;
  id: number;
  userId: number;
}
